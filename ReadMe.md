

# Django Channels Tutorial with [@abriev1116](https://gitlab.com/abriev1116)
## Django chanels yordamida chat app qilishni o'rganamiz

### O'ylaymanki bu loyiha siga qiziq bo'ladi 
### Loyihani clone qilib oling

#### 1 Virtual muhit yarating (venv)

$ python3 -m venv venv && source venv/bin/activate

#### 2 Requirementlarni o'rnating:

(venv)$ pip install -r requirements.txt
#### 3 Migratsiya :

(venv)$ python manage.py migrate

#### 4 Redis serverni ishga tushiring:

(venv)$ docker run -p 6379:6379 -d redis:5

#### 5 Serverni run qiling:

(venv)$ python manage.py runserver

#### 6 Odatiy tarzda autentifikatsiya uchun superuser yarating

(venv)$ python manage.py createsuperuser

#### 7 http://localhost:8000/admin/  quyidagi url manzil orqali avtorizatsiyadan oting.

#### 8 http://localhost:8000/chat/  avtorizatsiydan so'ng quyidagi url manzil orqali chatni ishga tushuishinggiz mumkin